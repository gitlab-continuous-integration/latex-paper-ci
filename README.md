# GITLAB CI - Paper Latex
Este proyecto posee una implementación de Integración Continua
para procesar un archivo `.tex`, el cual es un paper desarrollado
en equipo.

Los resultados de los _jobs_  se encuentran como _artifacts_ en cada
pipeline. Adicional, se despliegue el paper en `.pdf` a GitLab Pages.


## El despliegue
Para desplegar en GitLab Pages, es necesario guardar el resultado de un job llamado `pages`
como artifact en la ruta `public/`.

[Paper publicado](https://gitlab-continuous-integration.gitlab.io/latex-paper-ci/paper.pdf)

__Obs:__ en el link original del page devolverá un error 404 debido a que no se ha guardado
ningún archivo `index.html`, pero sí se puede acceder agregando el nombre del archivo `.pdf`
al final de la URL para ver el PDF en el navegador.

`http://` + `ruta-de-pages-del-proyecto` + `/archivo.pdf`


## Sobre el paper
Este paper fue desarrollado como parte de un curso de Ciencias de la Computación.
Créditos correspondientes a los autores especificados.


## Sobre el archivo CI
La configuración de la integración continua se obtuvo de la siguiente fuente:

Vipin Ajayakumar - [Continuous Integration of Latex projects with GitLab Pages](https://www.vipinajayakumar.com/continuous-integration-of-latex-projects-with-gitlab-pages.html)